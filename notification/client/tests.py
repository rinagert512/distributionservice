from rest_framework.test import APITestCase
import json

from .models import Client

base_url = "http://localhost:8000/client/"

class ClientTests(APITestCase):
    def test_create_client(self):
        url = base_url + "add/"
        data = {
            "phone_number": "78761212218",
            "tag": "group_1",
            "timezone": "UTC"
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 200)
        instance = Client.objects.get(phone_number="78761212218")
        self.assertEqual(instance.tag, "group_1")
        instance.delete()


    def test_update_client(self):
        url = base_url + "add/"
        data = {
            "phone_number": "78761212218",
            "tag": "group_1",
            "timezone": "UTC"
        }
        self.client.post(url, data, format='json')
        instance = Client.objects.get(phone_number="78761212218")
        client_id = instance.id

        url = base_url + "update/"
        data = {
            "client_id": client_id,
            "tag": "group_tests"
        }
        self.client.post(url, data, format='json')
        instance = Client.objects.get(id=client_id)
        self.assertEqual(instance.tag, "group_tests")
        instance.delete()

    def test_delete_client(self):
        url = base_url + "add/"
        data = {
            "phone_number": "78761212218",
            "tag": "group_1",
            "timezone": "UTC"
        }
        self.client.post(url, data, format='json')
        instance = Client.objects.get(phone_number="78761212218")
        client_id = instance.id

        url = base_url + "delete/"
        data = {
            "client_id": client_id
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Client.objects.filter(id=client_id).count(), 0)
    
    def test_all_client(self):
        url = base_url + "all/"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data["data"]), Client.objects.all().count())
    
    def test_client_info(self):
        url = base_url + "add/"
        data = {
            "phone_number": "78761212218",
            "tag": "group_1",
            "timezone": "UTC"
        }
        self.client.post(url, data, format='json')
        instance = Client.objects.get(phone_number="78761212218")
        client_id = instance.id

        url = base_url + str(client_id) + "/"
        response = self.client.get(url)
        data = response.data["data"]

        self.assertEqual(instance.phone_number, data["phone_number"])
        self.assertEqual(instance.tag, data["tag"])
        self.assertEqual(instance.timezone, data["timezone"])
        instance.delete()
        