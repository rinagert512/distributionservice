from django.core.exceptions import ValidationError
from pytz import all_timezones

import re

def validate_number(number):
    reg = re.compile('7[0-9]{10}')
    if not reg.match(number) :
        raise ValidationError(f"Number \"{number}\" doesn't match")

def validate_timezone(timezone):
    if timezone and not timezone in all_timezones:
        raise ValidationError(f"Incorrect timezone: \"{timezone}\"")