from django.urls import path

from . import views

app_name = 'client'

urlpatterns = [
    path('add/', views.add_client),
    path('update/', views.update_client_data),
    path('delete/', views.delete_client),
    path("<int:client_id>/", views.show_client),
    path("all/", views.show_all_clients)
]