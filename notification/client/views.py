from .models import Client
from notification import logger
from .serializers import ClientSerializer
from .validators import validate_number, ValidationError

from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view
from rest_framework.decorators import parser_classes

from django.shortcuts import redirect
from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

ERROR_RESPONSE = openapi.Response(
    description="Incorrect request",
    examples={
        "application/json": {
            "status": "error",
            "message": "Some error description, which can help you to understand what's wrong"
        }
    })

@swagger_auto_schema(
    method='post', 
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT, 
        required=["phone_number", "tag", "timezone"],
        properties={
            'phone_number': openapi.Schema(type=openapi.TYPE_STRING, description='Phone number in format 7XXXXXXXXXX'),
            'tag': openapi.Schema(type=openapi.TYPE_STRING, description='Client tag'),
            'timezone': openapi.Schema(type=openapi.TYPE_STRING, description='Timezone from PyTz timezone list'),
        }),
    operation_description="Add client",
    responses={
        "200": openapi.Response(
            description="Successfuly added client",
            examples={
                "application/json": {
                    "status": "ok",
                    "message": "Successfuly created client",
                    "client_id": 3
                }
            }
        ),
        "400": ERROR_RESPONSE,
    })
@api_view(['POST'])
@parser_classes([JSONParser])
def add_client(request):
    try:
        serializer = ClientSerializer(data=request.data)
        
        if not serializer.is_valid():
            return Response({
                                "status": "error",
                                "data": serializer.errors
                            }, status=400)
        
        instance = serializer.save()
        
        logger.info(f"(client_id={instance.id}) CREATED")
        return Response({
                            "status": "ok",
                            "message": "Successfuly created client",
                            "client_id": instance.id
                        }, status = 200)
    except Exception as e:
        logger.info("EXCEPTION: " + str(e))
        return Response({
                            "status": "error", 
                            "message": "Validation error occured while processing your request."
                        }, status=400)

@swagger_auto_schema(
    method='post', 
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT, 
        required=["client_id"],
        properties={
            'client_id': openapi.Schema(type=openapi.TYPE_INTEGER, description='Id of client to delete'),
        }),
    operation_description="Delete client with specified id",
    responses={
        200: openapi.Response(
            description="Successfuly deleted client",
            examples={
                "application/json": {
                    "status": "ok",
                    "message": "Successfully deleted client"
                }
            }
        ),
        400: ERROR_RESPONSE,
    }
)
@api_view(["POST"])
@parser_classes([JSONParser])
def delete_client(request):
    if not request.data.get("client_id", False):
        return Response({"status": "error", "message": "You must provide client id"}, status=400)
    
    client_id = int(request.data["client_id"])
    client_set = Client.objects.filter(id=client_id)
    
    if not len(client_set):
        return Response({"status": "error", "message": "No such client"}, status=400)
    
    client = client_set[0]
    client.delete()

    logger.info(f"(client_id={client_id}) DELETED")
    return Response({"status": "ok", "message": "Successfully deleted client"}, status=200)

@swagger_auto_schema(
    method="get",
    operation_description="Show client info",
    responses={
        "200": openapi.Response(
            description="Client info",
            examples={
                "application/json": {
                    "status": "ok",
                    "data": {
                        "id": 1,
                        "phone_number": "79812345167",
                        "tag": "sfdfdsf",
                        "timezone": "UTC"
                    }
                }
            }
        ),
        "400": ERROR_RESPONSE,
    })
@api_view(["GET"])
def show_client(request, client_id):
    client_set = Client.objects.filter(id=client_id)
    
    if not len(client_set):
        return Response({"status": "error", "message": "No such client"}, status=400)
    
    client = client_set[0]
    serializer = ClientSerializer(client)
    return Response({"status": "ok", "data": serializer.data})

@swagger_auto_schema(
    method="get",
    operation_description="Show all clients",
    responses={
        "200": openapi.Response(
            description="Client info",
            examples={
                "application/json": {
                    "status": "ok",
                    "data": [
                        {
                            "id": 1,
                            "phone_number": "79812345167",
                            "tag": "sfdfdsf",
                            "timezone": "UTC"
                        }
                    ]
                }
            }
        ),
        "400": ERROR_RESPONSE,
    })
@api_view(["GET"])
def show_all_clients(request):
    client_set = Client.objects.all()
    serializer = ClientSerializer(client_set, many=True)
    return Response({"status": "ok", "data": serializer.data})


@swagger_auto_schema(
    method='post', 
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT, 
        required=["client_id"],
        properties={
            'client_id': openapi.Schema(type=openapi.TYPE_INTEGER, description='Id of client to update'),
            'phone_number': openapi.Schema(type=openapi.TYPE_STRING, description='Phone number in format 7XXXXXXXXXX'),
            'tag': openapi.Schema(type=openapi.TYPE_STRING, description='Client tag'),
            'timezone': openapi.Schema(type=openapi.TYPE_STRING, description='Timezone from PyTz timezone list'),
        }),
    operation_description="Update any of three fields of client with specified id",
    responses={
        302: openapi.Response(
            description="Redirect to client info"
        ),
        400: ERROR_RESPONSE,
    }
)
@api_view(["POST"])
def update_client_data(request):
    if not request.data.get("client_id", False):
        return Response({"status": "error", "message": "You must provide client id"}, status=400)
    
    client_id = int(request.data["client_id"])
    client_set = Client.objects.filter(id=client_id)
    
    if not len(client_set):
        return Response({"status": "error", "message": "No such client"}, status=400)
    
    client = client_set[0]

    if request.data.get("phone_number", False):
        client.phone_number = request.data["phone_number"]
    if request.data.get("timezone", False):
        client.timezone = request.data["timezone"]
    if request.data.get("tag", False):
        client.tag = request.data["tag"]
    
    try:
        client.full_clean()
        client.save()
        logger.info(f"(client_id={client.id}) UPDATED")
        return redirect(f"/client/{client.id}")
    except ValidationError as e:
        return Response({"status": "error", "message": e}, status=400)
    except Exception as e:
        logger.info("EXCEPTION: " + e)
        return Response({"status": "error", "message": "Occured error while updating client"}, status=400)