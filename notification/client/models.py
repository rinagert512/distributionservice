from django.db import models
from .validators import validate_number
from .validators import validate_timezone

class Client(models.Model):
    phone_number = models.CharField(verbose_name="Phone number", 
                                    max_length=11,
                                    validators=[validate_number],
                                    unique=True)

    mobile_operator_code = models.CharField(verbose_name="Mobile operator code",
                                            max_length=3)

    tag = models.CharField(verbose_name="Tag", blank=True, default="", max_length=20)

    timezone = models.CharField(verbose_name="Timezone", 
                                blank=True, 
                                default="",
                                validators=[validate_timezone],
                                max_length=20)

    class Meta:
        verbose_name = "Client"
        verbose_name_plural = "Clients"

    def __str__(self):
        return "Client {}".format(self.id)

    def save(self, *args, **kwargs):
        self.mobile_operator_code = self.phone_number[1:4]
        super(Client, self).save(*args, **kwargs)