# Generated by Django 4.1 on 2022-08-05 17:12

import client.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0002_alter_client_phone_number'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='client',
            options={'verbose_name': 'Client', 'verbose_name_plural': 'Clients'},
        ),
        migrations.AlterField(
            model_name='client',
            name='mobile_operator_code',
            field=models.CharField(max_length=3, verbose_name='Mobile operator code'),
        ),
        migrations.AlterField(
            model_name='client',
            name='phone_number',
            field=models.CharField(max_length=11, unique=True, validators=[client.validators.validate_number], verbose_name='Phone number'),
        ),
        migrations.AlterField(
            model_name='client',
            name='tag',
            field=models.CharField(blank=True, default='', max_length=20, verbose_name='Tag'),
        ),
        migrations.AlterField(
            model_name='client',
            name='timezone',
            field=models.CharField(blank=True, default='', max_length=20, validators=[client.validators.validate_timezone], verbose_name='Timezone'),
        ),
    ]
