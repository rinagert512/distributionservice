from __future__ import absolute_import, unicode_literals
from .celery import app as celery_app

import logging

logging.basicConfig(filename="log.log",
                    filemode='a',
                    format='[+] %(asctime)s:%(msecs)d - %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


__all__ = ('celery_app',)