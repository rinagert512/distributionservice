from importlib.metadata import distribution
from rest_framework.test import APITestCase
from dateutil import parser
import json

from .models import Distribution
from .models import Message

base_url = "http://localhost:8000/distribution/"

class DistributionTests(APITestCase):
    def test_create_distribution(self):
        url = base_url + "add/"
        data = {
            "date_start": "08-07-2022 1:58:35",
            "date_end": "08-20-2022 1:58:37",
            "text_message": "Kol common message",
            "filter_code": 891
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 200)
        instance = Distribution.objects.get(id=response.data["distribution_id"])
        self.assertEqual(instance.text_message, "Kol common message")
        instance.delete()


    def test_update_client(self):
        url = base_url + "add/"
        data = {
            "date_start": "08-07-2022 1:58:35",
            "date_end": "08-20-2022 1:58:37",
            "text_message": "Kol common message",
            "filter_code": 891
        }
        response = self.client.post(url, data, format='json')
        distribution_id = response.data["distribution_id"]

        url = base_url + "update/"
        data = {
            "distribution_id": distribution_id,
            "filter_tag": "group_tests"
        }
        self.client.post(url, data, format='json')
        instance = Distribution.objects.get(id=distribution_id)
        self.assertEqual(instance.filter_tag, "group_tests")
        instance.delete()

    def test_delete_distribution(self):
        url = base_url + "add/"
        data = {
            "date_start": "08-07-2022 1:58:35",
            "date_end": "08-20-2022 1:58:37",
            "text_message": "Kol common message",
            "filter_code": 891
        }
        response = self.client.post(url, data, format='json')
        distribution_id = response.data["distribution_id"]

        url = base_url + "delete/"
        data = {
            "distribution_id": distribution_id
        }
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Distribution.objects.filter(id=distribution_id).count(), 0)
    
    def test_all_distributions(self):
        url = base_url + "all/"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data["data"]), Distribution.objects.all().count())
    
    def test_distribution_info(self):
        url = base_url + "add/"
        data = {
            "date_start": "08-07-2022 1:58:35",
            "date_end": "08-20-2022 1:58:37",
            "text_message": "Kol common message",
            "filter_code": 891
        }
        response = self.client.post(url, data, format='json')
        distribution_id = response.data["distribution_id"]

        url = base_url + str(distribution_id) + "/"
        response = self.client.get(url)
        data = response.data["data"]

        instance = Distribution.objects.get(id=distribution_id)
        self.assertEqual(instance.date_start, parser.parse(data["date_start"]))
        self.assertEqual(instance.date_end, parser.parse(data["date_end"]))
        instance.delete()
    
    def test_statistics(self):
        url = base_url + "statistics/"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data["data"]), Distribution.objects.all().count())
    
    def test_messages(self):
        url = base_url + "add/"
        data = {
            "date_start": "08-07-2022 1:58:35",
            "date_end": "08-20-2022 1:58:37",
            "text_message": "Kol common message",
            "filter_code": 891
        }
        response = self.client.post(url, data, format='json')
        distribution_id = response.data["distribution_id"]
        distribution = Distribution.objects.get(id=distribution_id)

        url = base_url + f"{distribution_id}/messages/"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data["data"]), Message.objects.filter(distribution=distribution).count())