from notification.celery import app
from client.models import Client
from .models import Message
from .models import Distribution

from celery.exceptions import MaxRetriesExceededError
from celery.signals import after_setup_logger
from datetime import datetime
from pytz import timezone

import requests
import logging
import json
import os

logger = logging.getLogger(__name__)

@after_setup_logger.connect
def setup_loggers(logger, *args, **kwargs):
    formatter = logging.Formatter('[+] %(asctime)s:%(msecs)d - %(message)s')

    fh = logging.FileHandler('celery_logs.log')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

@app.task
def start_distribution(distribution_id):
    try:
        distribution = Distribution.objects.get(id=distribution_id)
        clients = Client.objects.all()
        if distribution.filter_code:
            clients = clients.filter(mobile_operator_code=distribution.filter_code)
        if distribution.filter_tag:
            clients = clients.filter(tag=distribution.filter_tag)
    except Exception as exc:
        logger.info("CELERY-TASK:START_DISTRIBUTION:ERROR: " + str(exc))
        return

    for client in clients:
        try:
            msg = Message(date_creation=datetime.now(timezone("Europe/Moscow")),
                        distribution=distribution,
                        client=client,
                        status="IN PROGRESS")
            msg.save()
            process_message.delay(msg.id)
        except Exception as exc:
            logger.info("CELERY-TASK:START_DISTRIBUTION:ERROR: " + str(exc))

@app.task(
    retry_backoff=20, 
    max_retries=10,
    retry_jitter=False
)
def process_message(msg_id):
    msg = Message.objects.get(id=msg_id)

    try:
        if datetime.now(timezone("Europe/Moscow")) > msg.distribution.date_end:
            msg.status = "FAILED"
            msg.save()
            logger.info(f"(message_id={msg.id}) EXPIRED")
            return

        url = f"https://probe.fbrq.cloud/v1/send/{msg.id}"
        data = {
            "id": msg.id,
            "phone": msg.client.phone_number,
            "text": msg.distribution.text_message
        }
        token = os.environ["JWT_TOKEN"]
        headers = {
            "Authorization": "Bearer " + token
        }

        resp = requests.post(url=url,
                             data=json.dumps(data),
                             headers=headers)
        
        logger.info(f"(message_id={msg.id}) got status_code={resp.status_code} and response:")
        logger.info(resp.text)

        if resp.status_code != 200:
            process_message.retry((msg_id,))
        else:
            logger.info(f"(message_id={msg.id}) SENDED")
            msg.status = "SENDED"
            msg.save()
    except MaxRetriesExceededError:
        msg.status = "FAILED"
        msg.save()
        logger.info(f"(message_id={msg.id}) MAX RETRY EXCEPTION")
    except Exception as exc:
        logger.info(f"(message_id={msg.id}) EXCEPTION: " + str(exc))
