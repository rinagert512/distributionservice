from django.contrib import admin
from .models import Distribution
from .models import Message

@admin.register(Distribution)
class DistributionAdmin(admin.ModelAdmin):
    pass

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    pass