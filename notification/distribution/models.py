from django.db import models
from django.utils import timezone

from client.models import Client

class Distribution(models.Model):
    date_start = models.DateTimeField(verbose_name="Start date", default=timezone.now)
    text_message = models.CharField(verbose_name="Text message", max_length=300)
    filter_code = models.CharField(verbose_name="Filter code", max_length=3, blank=True, default="")
    filter_tag = models.CharField(verbose_name="Filter tag", max_length=20, blank=True, default="")
    date_end = models.DateTimeField(verbose_name="End date")

    class Meta:
        verbose_name = "Distribution"
        verbose_name_plural = "Distributions"

    def __str__(self):
        return "Distribution {}".format(self.id)

class Message(models.Model):
    STATUS_CHOICES = (
        ("IN PROGRESS", "in progress"),
        ("SENDED", "sended"),
        ("FAILED", "failed")
    )

    date_creation = models.DateTimeField(verbose_name="Creation date", default=timezone.now)
    distribution = models.ForeignKey(Distribution, verbose_name="Distribution", on_delete=models.CASCADE)
    client = models.ForeignKey(Client, verbose_name="Client", on_delete=models.SET_NULL, null=True)
    status = models.CharField(verbose_name="Status", max_length=20, choices=STATUS_CHOICES, default="SENDED")

    class Meta:
        verbose_name = "Message"
        verbose_name_plural = "Messages"

    def __str__(self):
        return "Message {}".format(self.id)