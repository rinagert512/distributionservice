from datetime import datetime
from .models import Distribution
from .models import Message
from notification import logger
from .serializers import DistributionSerializer, MessageSerializer
from .tasks import start_distribution

from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view
from rest_framework.decorators import parser_classes

from django.shortcuts import redirect
from dateutil import parser
from pytz import timezone

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

ERROR_RESPONSE = openapi.Response(
    description="Incorrect request",
    examples={
        "application/json": {
            "status": "error",
            "message": "Some error description, which can help you to understand what's wrong"
        }
    })

@swagger_auto_schema(
    method='post', 
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT, 
        required=["date_start", "date_end", "text_message"],
        properties={
            'date_start': openapi.Schema(type=openapi.TYPE_STRING, description='Datetime in MSK format (like 08-07-2022 1:58:35)'),
            'date_end': openapi.Schema(type=openapi.TYPE_STRING, description='Datetime in MSK format (like 08-07-2022 1:58:35)'),
            'text_message': openapi.Schema(type=openapi.TYPE_STRING, description='Text message'),
            'filter_code': openapi.Schema(type=openapi.TYPE_INTEGER, description='Mobile operator code filter'),
            'filter_tag': openapi.Schema(type=openapi.TYPE_STRING, description='Tag filter'),
        }),
    operation_description="Add distribution. You must set at least one filter",
    responses={
        "200": openapi.Response(
            description="Successfuly added distribution",
            examples={
                "application/json": {
                    "status": "ok",
                    "message": "Successfuly created distribution",
                    "distribution_id": 3
                }
            }
        ),
        "400": ERROR_RESPONSE,
    }
)
@api_view(['POST'])
@parser_classes([JSONParser])
def add_distribution(request):
    try:
        data = request.data
        try:
            data["date_start"] = timezone("Europe/Moscow").localize(parser.parse(data["date_start"]))
            data["date_end"] = timezone("Europe/Moscow").localize(parser.parse(data["date_end"]))
        except Exception as e:
            return Response({
                                "status": "error",
                                "message": str(e)
                            }, status=400)

        if data["date_start"] >= data["date_end"]:
            return Response({
                                "status": "error",
                                "message": "Date start must be greater than date end"
                            }, status=400)

        if data["date_end"] <= datetime.now(timezone("Europe/Moscow")):
            return Response({
                                "status": "error",
                                "message": "Date end must be greater than current time"
                            }, status=400)

        serializer = DistributionSerializer(data=data)
        
        if not serializer.is_valid():
            return Response({
                                "status": "error",
                                "data": serializer.errors
                            }, status=400)
        
        if not (data.get("filter_code", False) or data.get("filter_tag", False)):
            return Response({
                                "status": "error",
                                "message": "You must provide code or tag filter"
                            }, status=400)

        instance = serializer.save()
        start_distribution.apply_async((instance.id, ), eta=instance.date_start)

        logger.info(f"(distribution_id={instance.id}) CREATED")
        return Response({
                            "status": "ok",
                            "message": "Successfuly created distribution",
                            "distribution_id": instance.id
                        }, status = 200)
    except Exception as e:
        logger.info("EXCEPTION: " + str(e))
        return Response({
                            "status": "error", 
                            "message": "Validation error occured while processing your request."
                        }, status=400)

@swagger_auto_schema(
    method='post', 
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT, 
        required=["distribution_id"],
        properties={
            'distribution_id': openapi.Schema(type=openapi.TYPE_STRING, description='Id of distribution to delete'),
        }),
    operation_description="Delete distribution",
    responses={
        200: openapi.Response(
            description="Successfuly deleted distribution",
            examples={
                "application/json": {
                    "status": "ok",
                    "message": "Successfully deleted distribution"
                }
            }
        ),
        400: ERROR_RESPONSE,
    }
)
@api_view(["POST"])
@parser_classes([JSONParser])
def delete_distribution(request):
    if not request.data.get("distribution_id", False):
        return Response({"status": "error", "message": "You must provide distribution id"}, status=400)
    
    distribution_id = int(request.data["distribution_id"])
    distribution_set = Distribution.objects.filter(id=distribution_id)
    
    if not len(distribution_set):
        return Response({"status": "error", "message": "No such distribution"}, status=400)
    
    distribution = distribution_set[0]
    distribution.delete()

    logger.info(f"(distribution_id={distribution_id}) DELETED")
    return Response({"status": "ok", "message": "Successfully deleted distribution"}, status=200)

@swagger_auto_schema(
    method="get",
    operation_description="Show distribution info",
    responses={
        "200": openapi.Response(
            description="Distribution",
            examples={
                "application/json": {
                    "status": "ok",
                    "data": 
                    {
                        "id": 40,
                        "date_start": "2022-08-07T01:58:35+03:00",
                        "text_message": "Kol common message",
                        "filter_code": "891",
                        "filter_tag": "",
                        "date_end": "2022-08-07T23:58:37+03:00"
                    }
                }
            }
        ),
        "400": ERROR_RESPONSE,
    })
@api_view(["GET"])
def show_distribution(request, distribution_id):
    distribution_set = Distribution.objects.filter(id=distribution_id)
    
    if not len(distribution_set):
        return Response({"status": "error", "message": "No such distribution"}, status=400)
    
    distribution = distribution_set[0]
    serializer = DistributionSerializer(distribution)
    return Response({"status": "ok", "data": serializer.data})

@swagger_auto_schema(
    method="get",
    operation_description="Show all distributions",
    responses={
        "200": openapi.Response(
            description="All distribution",
            examples={
                "application/json": {
                    "status": "ok",
                    "data": [
                        {
                            "id": 40,
                            "date_start": "2022-08-07T01:58:35+03:00",
                            "text_message": "Kol common message",
                            "filter_code": "891",
                            "filter_tag": "",
                            "date_end": "2022-08-07T23:58:37+03:00"
                        }
                    ]
                }
            }
        ),
        "400": ERROR_RESPONSE,
    })
@api_view(["GET"])
def show_all_distributions(request):
    distribution_set = Distribution.objects.all()
    serializer = DistributionSerializer(distribution_set, many=True)
    return Response({"status": "ok", "data": serializer.data})

@swagger_auto_schema(
    method='post', 
    request_body=openapi.Schema(
        type=openapi.TYPE_OBJECT, 
        required=["distribution_id"],
        properties={
            'distribution_id': openapi.Schema(type=openapi.TYPE_STRING, description='Id of distribution to delete'),
            'date_start': openapi.Schema(type=openapi.TYPE_STRING, description='Datetime in MSK format (like 08-07-2022 1:58:35)'),
            'date_end': openapi.Schema(type=openapi.TYPE_STRING, description='Datetime in MSK format (like 08-07-2022 1:58:35)'),
            'text_message': openapi.Schema(type=openapi.TYPE_STRING, description='Text message'),
            'filter_code': openapi.Schema(type=openapi.TYPE_INTEGER, description='Mobile operator code filter'),
            'filter_tag': openapi.Schema(type=openapi.TYPE_STRING, description='Tag filter'),
        }),
    operation_description="Update fields of distribution with specified id",
    responses={
        302: openapi.Response(
            description="Redirect to distribution info"
        ),
        400: ERROR_RESPONSE,
    }
)
@api_view(["POST"])
def update_distribution_data(request):
    if not request.data.get("distribution_id", False):
        return Response({"status": "error", "message": "You must provide distribution id"}, status=400)
    
    distribution_id = int(request.data["distribution_id"])
    distribution_set = Distribution.objects.filter(id=distribution_id)
    
    if not len(distribution_set):
        return Response({"status": "error", "message": "No such distribution"}, status=400)
    
    distribution = distribution_set[0]

    if request.data.get("date_start", False):
        date_start = request.data["date_start"]
        try:
            date_start = parser.parse(date_start)
            distribution.date_start = date_start
        except:
            return Response({"status": "error", "message": "Couldn't parse start_date"})

    if request.data.get("date_end", False):
        date_end = request.data["date_end"]
        try:
            date_end = parser.parse(date_end)
            distribution.date_end = date_end
        except:
            return Response({"status": "error", "message": "Couldn't parse start_date"})

    if request.data.get("text_message", False):
        distribution.text_message = request.data["text_message"]
    if request.data.get("filter_code", False):
        distribution.filter_code = request.data["filter_code"]
    if request.data.get("filter_tag", False):
        distribution.filter_tag = request.data["filter_tag"]
    if request.data.get("remove_filter_code", False):
        distribution.filter_code = ""
    if request.data.get("remove_filter_tag", False):
        distribution.filter_tag = ""
    
    if not distribution.filter_tag and not distribution.filter_code:
        return Response({"status": "error", "message": "Should be at least one filter"})

    try:
        distribution.full_clean()
        distribution.save()
        logger.info(f"(distribution_id={distribution.id}) UPDATED")
        return redirect(f"/distribution/{distribution.id}")
    except ValidationError as e:
        return Response({"status": "error", "message": e}, status=400)
    except Exception as e:
        logger.info("EXCEPTION: " + e)
        return Response({"status": "error", "message": "Occured error while updating distribution"}, status=400)


@swagger_auto_schema(
    method="get",
    operation_description="Show messages of specified distribution",
    responses={
        "200": openapi.Response(
            description="Messages",
            examples={
                "application/json": {
                    "status": "ok",
                    "data": [
                        {
                            "id": 1,
                            "client_id": 5,
                            "distribution_id": 7,
                            "status": "SENDED"
                        }
                    ]
                }
            }
        ),
        "400": ERROR_RESPONSE,
    })
@api_view(["GET"])
def distribution_messages(request, distribution_id):
    distribution_set = Distribution.objects.filter(id=distribution_id)
    
    if not len(distribution_set):
        return Response({"status": "error", "message": "No such distribution"}, status=400)
    
    distribution = distribution_set[0]
    messages = Message.objects.filter(distribution=distribution)
    serializer = MessageSerializer(messages, many=True)
    return Response({"status": "ok", "data": serializer.data})


@swagger_auto_schema(
    method="get",
    operation_description="Show distribution statistics",
    responses={
        "200": openapi.Response(
            description="Distribution statistics",
            examples={
                "application/json": {
                    "status": "ok",
                    "data": {
                        39: {
                            "sended": 10,
                            "failed": 0,
                            "in_progress": 0
                        }
                    }
                }
            }
        ),
        "400": ERROR_RESPONSE,
    })
@api_view(["GET"])
def distribution_statistics(request):
    statistics = {}
    distributions = Distribution.objects.all()

    for distribution in distributions:
        statistics[distribution.id] = {
            "sended": len(Message.objects.filter(distribution=distribution, status="SENDED")),
            "failed": len(Message.objects.filter(distribution=distribution, status="FAILED")),
            "in_progress": len(Message.objects.filter(distribution=distribution, status="IN PROGRESS"))
        }
    
    return Response({"status": "ok", "data": statistics})
