from importlib.metadata import distribution
from rest_framework import serializers

from .models import Distribution
from .models import Message

class DistributionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Distribution
        fields = ["id", "date_start", "text_message", "filter_code", "filter_tag", "date_end"]

class MessageSerializer(serializers.ModelSerializer):
    distribution_id = serializers.ReadOnlyField(source='distribution.id')
    client_id = serializers.ReadOnlyField(source='client.id')

    class Meta:
        model = Message
        fields = ["id", "status", "client_id", "distribution_id"]