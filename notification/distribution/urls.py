from django.urls import path
from . import views

app_name = 'distribution'

urlpatterns = [
    path('add/', views.add_distribution),
    path('update/', views.update_distribution_data),
    path('delete/', views.delete_distribution),
    path("<int:distribution_id>/", views.show_distribution),
    path("all/", views.show_all_distributions),
    path("<int:distribution_id>/messages/", views.distribution_messages),
    path("statistics/", views.distribution_statistics),
]